const dgram = require('dgram')
const clc = require('cli-color')

const server = dgram.createSocket('udp4')

server.bind(4123, '0.0.0.0', () => {
	console.log('Server bound...', clc.yellow('port 4123'))
})

server.on('listening', () => {
	console.log('Server listening...')
})

server.on('close', () => {
	console.log('Server closed.')
})

server.on('message', (msg, rinfo) => {
	console.log(clc.green('got message'), msg, clc.white('=>'), clc.whiteBright(msg))
	console.log(clc.greenBright('message from'), rinfo.address)
	// server.close()
})
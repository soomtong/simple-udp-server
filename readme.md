## simple udp server and client with broadcast sample

### how to use

prepare node.js and npm. and clone it.

	$ git clone https://bitbucket.org/soomtong/simple-udp-server
	$ cd simple-udp-server
	$ npm install

and recommended nodemon, eslint for easy development environment

	$ sudo npm -g i nodemon eslint

### run server

	$ node simple-server.js

or

	$ nodemon simple-server.js

### run client 

	$ node uni-client.js

### run broadcast client

	$ node broad-client.js

## license

MIT © soomtong
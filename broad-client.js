const dgram = require('dgram')
const clc = require('cli-color')

const client = dgram.createSocket('udp4')
const broadcastAddress = '255.255.255.255'

client.bind(() => client.setBroadcast(true))

client.send('hello', 4123, broadcastAddress, (err, value) => {
	if (err) {
		console.error(err)
	} else {
		console.log(clc.green('sent message length:'), value)
	}
	client.close()
})
const dgram = require('dgram')
const clc = require('cli-color')

const client = dgram.createSocket('udp4')
const destAddress = 'localhost'

client.send('hello', 4123, destAddress, (err, value) => {
	if (err) {
		console.error(err)
	} else {
		console.log(clc.green('sent message length:'), value)
	}
	client.close()
})